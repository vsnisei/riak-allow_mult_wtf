package wtf; /**
 * Date: 12/13/13
 * Time: 8:58 PM
 */

import com.basho.riak.client.IRiakObject;
import com.basho.riak.client.RiakException;
import com.basho.riak.client.bucket.BucketProperties;
import com.basho.riak.client.builders.BucketPropertiesBuilder;
import com.basho.riak.client.convert.JSONConverter;
import com.basho.riak.client.raw.RawClient;
import com.basho.riak.client.raw.StoreMeta;
import com.basho.riak.client.raw.pbc.PBClientAdapter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class Main {


    private Main() {
    }

    static interface Performer {
        void perform();
    }

    public static void main(final String[] args) throws InterruptedException {
        if (args.length < 3) {
            System.err.println("Usage java -jar <this_jar.jar> host port batch_size");
        }

        final int THREADS = 5;
        final int WORK = Integer.parseInt(args[2]);

        final InetSocketAddress address = new InetSocketAddress(args[0], Integer.parseInt(args[1]));

        final ThreadLocalClientManager manager = new ThreadLocalClientManager(address);

        final String dummyValue = UUID.randomUUID().toString();
        final String bucket = UUID.randomUUID().toString();

        manager.withClient(new IResourceProcessor<RawClient, Object>() {
            @Override
            public Object process(final RawClient resource) throws IOException {
                final BucketProperties bucketProperties = resource.fetchBucket(bucket);
                final BucketPropertiesBuilder builder =
                        BucketPropertiesBuilder.from(bucketProperties)
                                .search(false)
                                .allowSiblings(true);
                resource.updateBucket(bucket, builder.build());
                return null;
            }
        });

        final Performer p  = new Performer() {
            @Override
            public void perform() {
                manager.withClient(new IResourceProcessor<RawClient, Object>() {
                    @Override
                    public Object process(final RawClient resource) throws IOException {
                        for (int z = 0; z < WORK; z++) {
                            final String id = "key-" + z;
                            final JSONConverter<String> converter = new JSONConverter<String>(String.class, bucket, id);
                            final IRiakObject obj = converter.fromDomain(dummyValue, null);// VClockUtil.getVClock(id));
                            resource.store(obj, StoreMeta.empty());
                        }
                        return null;
                    }
                });
            }
        };

        {
            final List<Map.Entry<Long, Long>> times = new CopyOnWriteArrayList<Map.Entry<Long, Long>>();
            final List<Thread> threads = new ArrayList<Thread>();
            final AtomicInteger fails = new AtomicInteger(0);
            final ConcurrentHashMap<Class, AtomicInteger> errstats = new ConcurrentHashMap<Class, AtomicInteger>();

            for (int i = 0; i < THREADS; i++) {
                final int k = i;
                threads.add(new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int j = k*WORK; j < (k+1)*WORK; j++) {
                            performWithStats(p, times, errstats, fails);
                        }
                    }
                }, "TEST-WORKER-"+k));
            }

            final long overallStart = System.currentTimeMillis();
            performWork(THREADS, WORK, threads);
            final long overallEnd = System.currentTimeMillis();
            System.err.println("THREADS JOINED");

            printStats(times, "WRITES");


            for (final Map.Entry<Class, AtomicInteger> e:errstats.entrySet()) {
                System.err.println(e.getKey().getName() +  " happened " + e.getValue().get() + " times");
            }

            System.err.println("---------- Other stats:");
            System.err.println("Overall errors: " + fails.get());
            final double overallApprox = (overallEnd - overallStart) / 1000.0;
            System.err.println("OverallTime: "+ overallApprox);
            System.err.println("Approx throughput: "+ (times.size()*1.0-fails.get()) / overallApprox + " responses/sec");
        }

        System.exit(0);
    }

    private static void performWithStats(final Performer p, final List<Map.Entry<Long, Long>> times, final ConcurrentHashMap<Class, AtomicInteger> errstats, final AtomicInteger fails) {
        try {
            final long start = System.currentTimeMillis();

            p.perform();

            final long diff = System.currentTimeMillis() - start;
            times.add(new ImmutablePair<Long, Long>(start, diff));
        } catch (final Exception e) {
            final Class aClass = e.getCause().getClass();
            errstats.putIfAbsent(aClass, new AtomicInteger(0));
            errstats.get(aClass).getAndIncrement();
            fails.getAndIncrement();
        }
    }

    private static void performWork(final int THREADS, final int WORK, final List<Thread> threads) throws InterruptedException {
        System.err.println("STARTING " + THREADS + " THREADS (BATCH SIZE: "+WORK+")");

        for (final Thread thread : threads) {
            thread.start();
        }
        System.err.println("JOINING THREADS");
        for (final Thread thread : threads) {
            thread.join();
        }
    }

    private static void printStats(final List<Map.Entry<Long, Long>> times, final String writes) {
        System.err.println("---------- Statistics for '" +writes+ "':");
        long meanTime = 0;
        long maxTime = 0;
        long minTime = Long.MAX_VALUE;
        final Map<Long, Long> reqs =  new HashMap<Long, Long>();

        for (final Map.Entry<Long, Long> time : times) {
            final long second = time.getKey() / 1000;
            if (!reqs.containsKey(second)) {
                reqs.put(second, 0L);
            }
            reqs.put(second, reqs.get(second) + 1);
            final Long value = time.getValue();
            meanTime += value;
            if (maxTime < value) {
                maxTime = value;
            }
            if (minTime > value) {
                minTime = value;
            }
        }


        System.err.println("Mean request time: "+ meanTime*1.0 / times.size() );
        System.err.println("Max request time: "+ maxTime );
        System.err.println("Min request time: "+ minTime );
        long meanReq = 0;
        for (final Long aLong : reqs.values()) {
            meanReq += aLong;
        }
        System.err.println("Throughput: "+ meanReq*1.0 / reqs.size() + " responses/sec");
        System.err.println("Total ops: "+ times.size());
        System.err.println();
    }

    static class ImmutablePair<K, V> implements Map.Entry<K, V> {

        private final K key;
        private final V val;

        ImmutablePair(final K key, final V val) {
            this.key = key;
            this.val = val;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return val;
        }

        @Override
        public V setValue(final V value) {
            throw new UnsupportedOperationException();
        }
    }

    static class ThreadLocalClientManager {
        final InetSocketAddress address;

        public ThreadLocalClientManager(final InetSocketAddress address) {
            this.address = address;
        }

        // per-thread client
        private final ThreadLocal<RawClient> clientThreadLocal = new ThreadLocal<RawClient>() {
            @Override
            protected RawClient initialValue() {
                try {
                    final PBClientAdapter adapter =
                            new PBClientAdapter(address.getHostName(), address.getPort());

                    // setting unique client Id
                    final byte[] cid = new byte[4];
                    new Random().nextBytes(cid);
                    adapter.setClientId(cid);

                    return adapter;
                } catch (final IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        };

        public <T> T withClient(final IResourceProcessor<RawClient, T> processor)  {
            try{
                return processor.process(clientThreadLocal.get());
            } catch (final IOException e) {
                throw new IllegalStateException(e);
            } catch (final RiakException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public interface IResourceProcessor<R, T> {
        T process(R resource) throws IOException, RiakException;
    }
}
