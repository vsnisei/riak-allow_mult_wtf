How to build:

	mvn clean package

How to run:

	java -jar target/riak-allow_mult-wtf-standalone.jar RIAK_HOST RIAK_PBC_PORT BATCH_SIZE

Example: 
	
	$ java -jar target/riak-allow_mult-wtf-standalone.jar shadow.nsk.dm 8087 20

This command runs 5 threads, each performs 20 writes values per key in bucket with allow_mult=true

Output with 20 values/key: 

STARTING 5 THREADS (BATCH SIZE: 20)
JOINING THREADS
THREADS JOINED
OverallTime: 4.364
Mean request time: 204.03
Max request time: 472
Min request time: 54
Overall errors: 0
Approx throughput: 22.914757103574704 responses/sec
Throughput: 20.0 responses/sec


Output with 40 values/key:

STARTING 5 THREADS (BATCH SIZE: 40)
JOINING THREADS
THREADS JOINED
OverallTime: 32.041
Mean request time: 771.015
Max request time: 1648
Min request time: 94
Overall errors: 0
Approx throughput: 6.24200243438095 responses/sec
Throughput: 6.0606060606060606 responses/sec

-------------------------------------------------
Conclusion: Looks like some x^2 algo inside Riak. 
-------------------------------------------------
